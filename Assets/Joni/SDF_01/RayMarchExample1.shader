﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
Shader "Custom/RayMarchExample1"
{
	//Source code https://www.alanzucconi.com/2016/07/01/raymarching/
	//https://www.alanzucconi.com/2016/07/01/surface-shading/
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		//name("display name", Range(min, max))
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
				float4 pos : SV_POSITION; // Clip space
				float3 wPos : TEXCOORD1; // World position
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float STEPS;
			float3 _Centre = float3(0,0,0);
			float _Radius = 10;

            v2f vert (appdata_full v)
            {
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.wPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
            }
			//SPHERE hit
			bool sphereHit(float3 p)
			{
				return distance(p, _Centre) < _Radius;
			}
			float sphereDistance(float3 p)
			{
				return distance(p, _Centre) - _Radius;
			}
			//RayMarch
			fixed4 raymarch(float3 position, float3 direction)
			{
				for (int i = 0; i < STEPS; i++)
				{
					float distance = sphereDistance(position);
					if (distance < MIN_DISTANCE)
						return i / (float)STEPS;

					position += distance * direction;
				}
				return 0;
			}

            fixed4 frag (v2f i) : SV_Target
            {
				 float3 worldPosition = i.wPos;
				 float3 viewDirection = normalize(i.wPos - _WorldSpaceCameraPos);
				 return raymarch(worldPosition, viewDirection);
            }
            ENDCG
        }
    }
}
