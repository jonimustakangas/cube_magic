﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Raymarch Example"
{
	Properties
	{

		/*_CamPos("_CamPos", Vector) = (0,0,0,0)
		_CamRight("CamRight", Vector) = (1,0,0,0)
		_CamUp("_CamUp", Vector) = (0,1,0,0)
		_CamForward("_CamForward", Vector) = (0,0,0,0)
		_AspectRatio("_AspectRatio", Float) = 1
		_FieldOfView("_FieldOfView", Float) = 1*/
	}
	//r1("r1", float) = 1.0
		SubShader
	{
		  Pass
		  {
		//Blend SrcAlpha OneMinusSrcAlpha
		Blend SrcAlpha Zero

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		struct v2f
		{
		  float2 uv : TEXCOORD0;
		  float4 vertex : SV_POSITION;
		};

		v2f vert(appdata_base v)
		{
		  v2f o;
		  o.vertex = UnityObjectToClipPos(v.vertex);
		  o.uv = v.texcoord;
		  return o;
		}
		float sdSphere(float3 p, float s) {
			return length(p) - s;
		}
		float sdBox(float3 p, float3 b) {
			float3 d = abs(p) - b;
			return min(max(d.x, max(d.y, d.z)),0.0) + length(max(d, 0.0));
		}
		float opS(float d1, float d2)
		{
			return max(-d1, d2);
		}
		float distFunc(float3 pos)
		{
			//float sphereRadius = 1;
			  float box = sdBox(pos - float3(0, 0, 0), float3(2, 2, 2));
			  float sphere = sdSphere(pos - float3(0, 0, 0), 2.5);
			  pos.z = fmod(pos.z + 2,4) - 2;
			  //return min(sphere,box);//Box and spherer
			  return opS(sphere, box);
			  //return length(pos) - sphereRadius;
				//length(sdSphere(pos, 1));
			}
			fixed4 renderSurface(float3 pos)
			{
			  const float2 eps = float2(0.0, 0.05);

			  float ambientIntensity = 0.01;
			  //float3 lightDir = float3(0, -0.5, 0.5);//This needs sun direction
			  //_WorldSpaceLightPos0
			  //float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
			  float3 lightDir = normalize(_WorldSpaceLightPos0.xyz*-1);
			  float3 normal = normalize(float3(
				distFunc(pos + eps.yxx) - distFunc(pos - eps.yxx),
				distFunc(pos + eps.xyx) - distFunc(pos - eps.xyx),
				distFunc(pos + eps.xxy) - distFunc(pos - eps.xxy)));

			  float diffuse = ambientIntensity + max(dot(-lightDir, normal), 0);

			  return fixed4(diffuse, diffuse, diffuse, 1);
			}
			// New Code
			float3 _CamPos;
			float3 _CamRight;
			float3 _CamUp;
			float3 _CamForward;

			float _AspectRatio;
			float _FieldOfView;

			fixed4 frag(v2f i) : SV_Target
			{
			  float2 uv = (i.uv - 0.5) * _FieldOfView;
			  //uv.x *= _AspectRatio;
			  float3 pos = _CamPos;
			  float3 ray = (_CamUp*-1) * uv.y + (_CamRight*-1) * uv.x + _CamForward;
			  /*fixed4 frag(v2f i) : SV_Target
			  {
				float2 uv = i.uv - 0.5;
				float3 camUp = float3(0, 1, 0);
				float3 camForward = float3(0, 0, 1);
				float3 camRight = float3(1, 0, 0);

				float3 pos = _WorldSpaceCameraPos;
				float3 ray = camUp * uv.y + camRight * uv.x + camForward;*/

				fixed4 color = 0;

				for (int i = 0; i < 10; i++)
				{
				  float d = distFunc(pos);

				  if (d < 0.01)
				  {
					  pos.z = fmod(pos.z + 2, 4) - 2;
					  pos.y = fmod(pos.y + 2, 4) - 2;
					color = renderSurface(pos);
					break;
				  }

				  pos += ray * d;
				  pos.z = fmod(pos.z + 2, 4) - 2;
				  pos.y = fmod(pos.y + 2, 4) - 2;
				  if (d > 500)
				  {
					  pos.z = fmod(pos.z + 2, 4) - 2;
					  pos.y = fmod(pos.y + 2, 4) - 2;
					  break;
				  }
				}

				return color;
			  }
			  ENDCG
			}
	}
}