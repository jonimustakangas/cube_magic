﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/RaymarchExample2"
{
	Properties
	{
		_Radius("Radius", float) = 1
		_Centre("Centre", Float) = 1
	}
	//r1("r1", float) = 1.0
	SubShader
	{
		Pass
		{
		//Blend SrcAlpha OneMinusSrcAlpha
		Blend SrcAlpha Zero

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#define STEPS 64
		#define STEP_SIZE 0.01
		float _Radius;
		float _Centre;
		struct v2f
		{
		  //float2 uv : TEXCOORD0; // UV 
		  //float4 vertex : SV_POSITION; //Vert Position
		  float4 pos : SV_POSITION; //Clip space
		  float4 wPos : TEXCOORD2; //World position
		};

		v2f vert(appdata_full v)
		{
		  v2f o;
		  //o.pos = mul(UNITY_MATRIX_MPV, v.vertex);
		  o.pos = UnityObjectToClipPos(v.vertex);
		  o.wPos = mul(unity_ObjectToWorld, v.vertex);
		  return o;
		}
		float sdSphere(float3 p, float s) {
			return length(p) - s;
		}
		float sdBox(float3 p, float3 b) {
			float3 d = abs(p) - b;
			return min(max(d.x, max(d.y, d.z)),0.0) + length(max(d, 0.0));
		}
		float opS(float d1, float d2)
		{
			return max(-d1, d2);
		}
		float distFunc(float3 pos)
		{
			//float sphereRadius = 1;
			  float box = sdBox(pos - float3(0, 0, 0), float3(2, 2, 2));
			  float sphere = sdSphere(pos - float3(0, 0, 0), 2.5);
			  pos.z = fmod(pos.z + 2,4) - 2;
			  //return min(sphere,box);//Box and spherer
			  return opS(sphere, box);
			  //return length(pos) - sphereRadius;
				//length(sdSphere(pos, 1));
		}
		bool sphereHit(float3 p)
		{
			return distance(p, _Centre) < _Radius;
		}
		bool raymarchHit(float3 position, float3 direction)
		{
			for (int i = 0; i < STEPS; i++)
			{
				if (sphereHit(position))
					return true;
				position += direction * STEP_SIZE;
			}
			return false;
		}
		fixed4 frag(v2f i) : SV_Target
		{
			//float3 viewDirection = normalize(i.wPos - _WorldSpaceCameraPos);
			float3 worldPosition = i.pos;
			float3 viewDirection = i.wPos;
				if (raymarchHit(worldPosition, viewDirection))
				{
					return fixed4(1, 0, 0, 1); // Red if hit the ball
				}
				else 
				{
				return fixed4(1, 1, 1, 1); // White otherwise
				}
			}
			ENDCG
		}
	}
}