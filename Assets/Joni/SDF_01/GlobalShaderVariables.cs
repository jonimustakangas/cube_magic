﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalShaderVariables : MonoBehaviour
{
    [SerializeField, Range(10, 500)]
    public int STEPS = 100;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
}
    private void OnPreRender()
    {
        Shader.SetGlobalVector("_CamPos", transform.position);
        Shader.SetGlobalVector("_CamRight", transform.right);
        Shader.SetGlobalVector("_CamUp", transform.up);
        Shader.SetGlobalVector("_CamForward", transform.forward);
        Shader.SetGlobalFloat("_AspectRatio", (float)Screen.width / (float)Screen.height);
        Shader.SetGlobalFloat("_FieldOfView", Mathf.Tan(Camera.main.fieldOfView * Mathf.Deg2Rad * 0.5f) * 2f);
        Shader.SetGlobalFloat("STEPS", STEPS);
    }
}
