﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSHellow : MonoBehaviour
{
    [SerializeField]
    ComputeShader compute;
    ComputeBuffer buffer;
    int[] arr = new int[5] {1,2,3,4,5};

    // Start is called before the first frame update
    void Start()
    {
        int size = sizeof(int);
        buffer = new ComputeBuffer(5,size);
        buffer.SetData(arr);

        compute.SetBuffer(compute.FindKernel("CSMain"), "arr", buffer);
        compute.Dispatch(0, arr.Length, 1, 1);

        buffer.GetData(arr);

        foreach (int i in arr)
        {
            Debug.Log(">" + i);
        }
        buffer.Release();
    }

}
