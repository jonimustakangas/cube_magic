﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Digger : MonoBehaviour
{
    public float sensitivity = 1f;
    public float speed = 15f;
    public float digSpeed = 1f;
    public float digRadius = 5f;
    public float digRadiusChangeSpeed = 1f;
    Vector2 rot = new Vector2();
    private float tempSens = 0;
    private void Awake()
    {
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
    }
    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * Input.GetAxis("Vertical") * speed * Time.deltaTime;
        transform.position += transform.right * Input.GetAxis("Horizontal") * speed * Time.deltaTime;

        rot.x -= Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime;
        rot.y += Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
        transform.rotation = Quaternion.Euler(rot.x, rot.y, 0);

        if (Input.GetMouseButton(0))
        {
            Add(digSpeed);
        }
        if (Input.GetMouseButton(1))
        {
            Add(-digSpeed);
        }
        digRadius += Input.mouseScrollDelta.y * digRadiusChangeSpeed;
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            float temp = sensitivity;
            sensitivity = tempSens;
            tempSens = temp;
        }
    }
    void Add(float multiplier = 1) {
        Ray camRay = Camera.main.ScreenPointToRay(new Vector3((float)Screen.width/2f, (float)Screen.height/2));
        RaycastHit hit;
        //Debug.Log("raycasting");
        if (Physics.Raycast(camRay, out hit,999f))
        {
            print("hit");
            FieldCreator field = hit.collider.GetComponent<FieldCreator>();
            if (field)
            {
                //print("field");
                field.PushBall(hit.point, multiplier * Time.deltaTime,digRadius);
            }
        }
    }
}
