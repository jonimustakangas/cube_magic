﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class FieldCreator : MonoBehaviour
{
    //check
    //http://paulbourke.net/geometry/polygonise/
    [Header("Common")]
    public int sideSize = 32;
    public float gridScale = 1f;
    protected float[,,] arr;
    int[,,] intArr;
    public float ballRadius = 0.1f;
    public bool drawGrid = false;
    public bool drawPoints = false;
    public RandomMode random;
    [Range(0.01f, 5f)]
    public float smoothingDistance = 1f;
    public Vector3 offset = Vector3.zero;
    public Vector3 timeOffsetScale = new Vector3(0, 0, 0);
    public Vector3 scale = new Vector3(0.2f, 1f, 0.2f);
    [Header("Random")]
    public float minValue = -32f;
    public float maxValue = 16f;
    [RangeAttribute(-32, 16)]
    public float visibilityTreshold = -10f;
    public enum RandomMode
    {
        Random, Perlin, Cube, Center, MultiBall, SinCos, Music, RandomBalls, PerlinCave
    }
    [Header("Multiball")]
    public bool reduceBalls = false;
    public float reduceAmount = 5f;
    public Vector4[] balls;
    //option for two sided meshes in sincos & music? just add Abs() before clamping?
    SoundFx soundFx;
    [Header("RandomBalls")]
    public int minBallCount = 10;
    public int maxBallCount = 20;
    public float minRadius = 5f;
    public float maxRadius = 15f;
    public float minAmount = 5f;
    public float maxAmount = 15f;
    [Header("Physics test")]
    public float spacing = 5f;
    public float radius = 2f;
    public float lifeTime = 20f;
    private Bounds bounds;
    private void Awake()
    {
        soundFx = GetComponent<SoundFx>();
    }
    void Start()
    {
        arr = new float[sideSize, sideSize, sideSize];
        intArr = new int[sideSize, sideSize, sideSize];
        CreateGrid();
    }
    private void Update()
    {
        CheckArraySize();
        CreateGrid();
        if (drawGrid)
        {
            DrawGrid();
        }
    }
    public void CreateGrid()
    {
        switch (random)
        {
            case RandomMode.Random:
                RandomToInts();//not randomizing every frame, just setting the visibilities
                break;
            case RandomMode.Perlin:
                PerlinGrid();
                break;
            case RandomMode.Cube:
                CubeGrid();
                break;
            case RandomMode.Center:
                CenterGrid();
                break;
            case RandomMode.MultiBall:
                MultiGrid();
                break;
            case RandomMode.SinCos:
                SinCosGrid();
                break;
            case RandomMode.Music:
                MusicGrid();
                break;
            case RandomMode.RandomBalls:
                RandomToInts();
                break;
            case RandomMode.PerlinCave:
                CaveGrid();
                break;
            default:
                Debug.LogError("not implemented randommode " + random);
                break;
        }
    }
    public void RandomGrid()
    {
        for (int x = 0; x < sideSize; x++)
        {
            for (int y = 0; y < sideSize; y++)
            {
                for (int z = 0; z < sideSize; z++)
                {
                    bool isOn = Random.Range(minValue, maxValue) > visibilityTreshold;
                    //arr[x, y, z] = Random.Range(minValue, maxValue);
                    arr[x, y, z] = isOn ? maxValue : minValue;
                }
            }
        }
        RandomToInts();
    }
    public void PerlinGrid()
    {
        for (int x = 0; x < sideSize; x++)
        {
            for (int z = 0; z < sideSize; z++)
            {
                Vector2 offsets = new Vector2(x, z);
                offsets += new Vector2(offset.x, offset.z);
                offsets += new Vector2(timeOffsetScale.x, timeOffsetScale.z) * Time.time;
                offsets.Scale(new Vector2(scale.x, scale.z));
                //float perlin = Mathf.PerlinNoise((x + offset.x) * scale.x, (z + offset.z) * scale.z);
                float perlin = Mathf.PerlinNoise(offsets.x, offsets.y);
                //perlin = (perlin + offset.y) % 1;//maybe
                perlin += offset.y;
                perlin *= scale.y;
                perlin += (1f - scale.y) / 2f;

                for (int y = 0; y < sideSize; y++)
                {
                    float basePoint = perlin - smoothingDistance;
                    float diff = (float)y / sideSize - perlin;
                    float yPos = (float)y / sideSize;
                    float t = Mathf.Clamp01((perlin - yPos) / smoothingDistance);
                    float value = LerpValue(t);
                    arr[x, y, z] = value;
                    intArr[x, y, z] = value > visibilityTreshold ? 1 : 0;
                }
            }
        }
    }
    public void CubeGrid()
    {
        int padding = 2;
        for (int x = 0; x < sideSize; x++)
        {
            for (int y = 0; y < sideSize; y++)
            {
                for (int z = 0; z < sideSize; z++)
                {
                    if (x < padding || x > sideSize - padding - 1 ||
                        y < padding || y > sideSize - padding - 1 ||
                        z < padding || z > sideSize - padding - 1)
                    {
                        intArr[x, y, z] = 0;
                        arr[x, y, z] = minValue;
                    }
                    else
                    {
                        intArr[x, y, z] = 1;
                        arr[x, y, z] = maxValue;
                    }
                }
            }
        }
    }
    public void CenterGrid()
    {
        float radius = (sideSize - 1) / 2f;
        Vector3 center = Vector3.one * radius;
        for (int x = 0; x < sideSize; x++)
        {
            for (int y = 0; y < sideSize; y++)
            {
                for (int z = 0; z < sideSize; z++)
                {
                    //float distance = Mathf.Abs(radius-)
                    Vector3 point = new Vector3(x, y, z);
                    Vector3 toCenter = center - point;
                    float lerp = toCenter.magnitude / radius;
                    float toMax = maxValue - minValue;
                    float value = maxValue - toMax * lerp;
                    arr[x, y, z] = value;
                    intArr[x, y, z] = value > visibilityTreshold ? 1 : 0;
                }
            }
        }
    }
    public void MultiGrid()
    {
        ClearArrs();
        for (int x = 0; x < sideSize; x++)
        {
            for (int y = 0; y < sideSize; y++)
            {
                for (int z = 0; z < sideSize; z++)
                {
                    foreach (var v4 in balls)
                    {
                        float radius = v4.w > 0 ? v4.w : 1;
                        Vector3 center = new Vector3(v4.x, v4.y, v4.z);
                        Vector3 point = new Vector3(x, y, z);
                        Vector3 toCenter = center - point;
                        float lerp = toCenter.magnitude / radius;
                        float toMax = maxValue - minValue;
                        float value = maxValue - toMax * lerp;
                        float relativeValue = value - minValue;
                        if (relativeValue > 0)
                        {
                            arr[x, y, z] += relativeValue;
                        }
                    }
                }
            }
        }
        RandomToInts();
    }
    public void SinCosGrid()
    {
        for (int x = 0; x < sideSize; x++)
        {
            for (int z = 0; z < sideSize; z++)
            {
                Vector2 offsets = new Vector2(x, z);//base grid coords
                offsets += new Vector2(offset.x, offset.z);//offsetting by user given amount
                offsets += new Vector2(timeOffsetScale.x, timeOffsetScale.z) * Time.time;//offsetting by axis scaled time
                //float sinCos = Mathf.Sin((x + offset.x) * scale.x) + Mathf.Cos((z + offset.z) * scale.z);
                float sinCos = Mathf.Sin((offsets.x) * scale.x) + Mathf.Cos((offsets.y) * scale.z);
                sinCos += 2;
                sinCos /= 4;
                sinCos += offset.y;
                sinCos *= scale.y;
                sinCos += (1f - scale.y) / 2f;//centers the scaled graph back to 0.5
                for (int y = 0; y < sideSize; y++)
                {
                    float basePoint = sinCos - smoothingDistance;
                    float diff = (float)y / sideSize - sinCos;
                    float yPos = (float)y / sideSize;
                    float t = Mathf.Clamp01((sinCos - yPos) / smoothingDistance);
                    float value = LerpValue(t);
                    arr[x, y, z] = value;
                    intArr[x, y, z] = value > visibilityTreshold ? 1 : 0;

                    //if ((float)y / sideSize < sinCos)
                    //{
                    //    intArr[x, y, z] = 1;
                    //    arr[x, y, z] = maxValue;
                    //}
                    //else
                    //{
                    //    intArr[x, y, z] = 0;
                    //    arr[x, y, z] = minValue;
                    //}
                }
            }
        }
        if (reduceBalls)
        {
            foreach (var v in balls)
            {
                PushBall(new Vector3(v.x, v.y, v.z), -reduceAmount, v.w);
            }
        }
    }
    public void MusicGrid()
    {
        if (soundFx.smoothedHistory.Count == 0)
        {
            return;
        }
        for (int x = 0; x < sideSize; x++)
        {
            for (int z = 0; z < sideSize; z++)
            {
                //make a version that lerps every value between datapoints both in spectrum and history

                //int zI = Mathf.FloorToInt((float)z / sideSize * soundFx.smoothedHistory.Count);
                int zI = soundFx.smoothedHistory.Count - 1 - z;
                if (zI < 0)
                {
                    zI = 0;
                }
                int xI = Mathf.FloorToInt((float)x / sideSize * soundFx.smoothedHistory[zI].Length);
                //xI = soundFx.smoothedHistory[zI].Length - 1 -x;
                float sample = (soundFx.smoothedHistory[zI][xI] + 1) / 2;//0-1 sample value
                for (int y = 0; y < sideSize; y++)
                {
                    float yPos = (float)y / sideSize;
                    float t = Mathf.Clamp01((sample - yPos) / smoothingDistance);
                    float value = LerpValue(t);
                    arr[x, y, z] = value;
                    intArr[x, y, z] = value > visibilityTreshold ? 1 : 0;
                }
            }
        }
    }
    public void RandomBallGrid()
    {
        ClearArrs();
        int ballCount = Random.Range(minBallCount, maxBallCount + 1);
        while (ballCount > 0)
        {
            float randomRadius = Random.Range(minRadius, maxRadius);
            Vector4 newBall = new Vector4(Random.Range(0, sideSize), Random.Range(0, sideSize), Random.Range(0, sideSize), randomRadius);
            float randomAmount = Random.Range(minAmount, maxAmount);
            PushBall(newBall, randomAmount, randomRadius);
            ballCount--;
        }
    }
    public void CaveGrid()
    {
        for (int x = 0; x < sideSize; x++)
        {
            for (int z = 0; z < sideSize; z++)
            {
                for (int y = 0; y < sideSize; y++)
                {
                    if (x == 0 && y == 0 && z == 0)
                    {
                        arr[x, y, z] = minValue;
                        intArr[x, y, z] = 0;
                        continue;
                    }
                    Vector2 offsets = new Vector2(x, z);
                    offsets += new Vector2(offset.x, offset.z);
                    offsets += new Vector2(timeOffsetScale.x, timeOffsetScale.z) * Time.time;
                    offsets.Scale(new Vector2(scale.x, scale.z));
                    float perlin = Mathf.PerlinNoise(offsets.x, offsets.y);
                    //perlin += offset.y;
                    //perlin *= scale.y;
                    //perlin += (1f - scale.y) / 2f;

                    Vector2 zOffsets = new Vector2(x, y);
                    zOffsets += new Vector2(offset.x, offset.y);
                    zOffsets += new Vector2(timeOffsetScale.x, timeOffsetScale.y) * Time.deltaTime;
                    zOffsets.Scale(new Vector2(scale.x, scale.y));
                    float zPerlint = Mathf.PerlinNoise(zOffsets.x, zOffsets.y);
                    float combine = (perlin + zPerlint) / 2f;
                    float value = LerpValue(combine);
                    //lerp to min based on distance to edge?
                    float xDistanceToEdge = x > sideSize / 2 ? sideSize - 1 - x : x;
                    float yDistanceToEdge = y > sideSize / 2 ? sideSize - 1 - y : y;
                    float zDistanceToEdge = z > sideSize / 2 ? sideSize - 1 - z : z;
                    float minDistanceToEdge = xDistanceToEdge < yDistanceToEdge ? xDistanceToEdge : yDistanceToEdge;
                    minDistanceToEdge = minDistanceToEdge < zDistanceToEdge ? minDistanceToEdge : zDistanceToEdge;
                    minDistanceToEdge *= gridScale;
                    float distanceT = Mathf.Clamp01(minDistanceToEdge / smoothingDistance);
                    value = Mathf.Lerp(minValue, value, distanceT);

                    arr[x, y, z] = value;
                    intArr[x, y, z] = value > visibilityTreshold ? 1 : 0;
                }
            }
        }
    }
    void ClearArrs()
    {
        for (int x = 0; x < sideSize; x++)
        {
            for (int y = 0; y < sideSize; y++)
            {
                for (int z = 0; z < sideSize; z++)
                {
                    arr[x, y, z] = minValue;
                    intArr[x, y, z] = 0;
                }
            }
        }
    }
    private void OnValidate()
    {
        if (UnityEditor.EditorApplication.isPlaying)
        {
            if (random == RandomMode.Random)
            {
                RandomToInts();
            }
        }
    }
    void RandomToInts()
    {
        for (int x = 0; x < sideSize; x++)
        {
            for (int y = 0; y < sideSize; y++)
            {
                for (int z = 0; z < sideSize; z++)
                {
                    if (arr[x, y, z] >= visibilityTreshold)
                    {
                        intArr[x, y, z] = 1;
                    }
                    else
                    {
                        intArr[x, y, z] = 0;
                    }
                }
            }
        }
    }
    private void OnDrawGizmos()
    {
        if (drawPoints && EditorApplication.isPlaying)
        {
            for (int x = 0; x < sideSize; x++)
            {
                for (int y = 0; y < sideSize; y++)
                {
                    for (int z = 0; z < sideSize; z++)
                    {
                        if (intArr[x, y, z] == 1)
                        {
                            Color minColor = Color.red;
                            Color maxColor = Color.green;
                            float current = arr[x, y, z];
                            float t = (current - minValue) / (maxValue - minValue);
                            Gizmos.color = Color.Lerp(minColor, maxColor, t);
                            Vector3 offset = Vector3.right * x + Vector3.up * y + Vector3.forward * z;
                            offset *= gridScale;
                            Gizmos.DrawSphere(transform.position + offset, ballRadius * gridScale);
                        }
                    }
                }
            }
        }
    }
    private void CheckArraySize()
    {
        if (arr.GetLength(0) != sideSize)
        {
            arr = new float[sideSize, sideSize, sideSize];
            UpdateBounds();
        }
        if (intArr.GetLength(0) != sideSize)
        {
            intArr = new int[sideSize, sideSize, sideSize];
            UpdateBounds();
        }
    }
    void DrawGrid()
    {
        int[,] drawArr = new int[sideSize, sideSize];
        //get max y
        for (int x = 0; x < sideSize; x++)
        {
            for (int z = 0; z < sideSize; z++)
            {
                int maxY = 0;
                for (int y = 0; y < sideSize; y++)
                {
                    if (intArr[x, y, z] == 1)
                    {
                        maxY = y;
                    }
                }
                drawArr[x, z] = maxY;
            }
        }
        //draw em
        for (int x = 0; x < sideSize; x++)
        {
            for (int z = 0; z < sideSize; z++)
            {
                int maxY = drawArr[x, z];
                Vector3 current = GetArrayPos(x, maxY, z);

                if (x != sideSize - 1)
                {
                    maxY = drawArr[x + 1, z];
                    Vector3 nextX = GetArrayPos(x + 1, maxY, z);
                    Debug.DrawLine(current, nextX, Color.green);
                }
                if (z != sideSize - 1)
                {
                    maxY = drawArr[x, z + 1];
                    Vector3 nextZ = GetArrayPos(x, maxY, z + 1);
                    Debug.DrawLine(current, nextZ, Color.green);
                }
            }
        }
    }
    public Vector3 GetArrayPos(int x, int y, int z, bool local = false)
    {
        Vector3 offset = Vector3.right * x + Vector3.up * y + Vector3.forward * z;
        offset *= gridScale;
        if (local)
        {
            return offset;
        }
        return transform.position + offset;
    }
    public Vector3Int GetArrayIndex(Vector3 position)
    {
        Vector3Int indexes = Vector3Int.zero;
        Debug.LogError("getarrayindex not implemented, returning zero");
        return indexes;
    }
    public float[,,] GetArr()
    {
        return arr;
    }
    public int[,,] GetIntArr()
    {
        return intArr;
    }
    public void PopulatePhysics()
    {
        Vector3 min = GetArrayPos(0, 0, 0);
        Vector3 max = GetArrayPos(sideSize - 1, sideSize - 1, sideSize - 1);
        float scaledSide = sideSize * gridScale;
        float scaledSpacing = spacing * gridScale;

        for (float x = 0; x < scaledSide; x += scaledSpacing)
        {
            for (float z = 0; z < scaledSide; z += scaledSpacing)
            {
                Vector3 offset = new Vector3(x, scaledSide, z);
                GameObject newBall = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                newBall.AddComponent<Rigidbody>();
                newBall.transform.position = transform.position + offset;
                Destroy(newBall, lifeTime);
            }
        }
    }
    public void PushBall(Vector3 position, float amount, float radius = 5f)
    {
        Vector3 center = position;
        for (int x = 1; x < sideSize - 1; x++)
        {
            for (int y = 1; y < sideSize - 1; y++)
            {
                for (int z = 1; z < sideSize - 1; z++)
                {
                    //float distance = Mathf.Abs(radius-)
                    Vector3 point = GetArrayPos(x, y, z);
                    Vector3 toCenter = center - point;
                    if (toCenter.magnitude <= radius)
                    {
                        //print("adding value to x" + x + " y" + y + " z" +z);
                        float lerp = Mathf.Clamp01(1 - (toCenter.magnitude / radius));
                        arr[x, y, z] = Mathf.Clamp(arr[x, y, z] + lerp * amount, minValue, maxValue);
                        intArr[x, y, z] = arr[x, y, z] > visibilityTreshold ? 1 : 0;
                    }
                }
            }
        }
    }
    public void UpdateBounds()
    {
        bounds.min = transform.position;
        bounds.max = Vector3.one * sideSize * gridScale;
    }
    public Bounds GetBounds()
    {
        return bounds;
    }
    public float LerpValue(float t)
    {
        return minValue + (maxValue - minValue) * t;
    }
}
