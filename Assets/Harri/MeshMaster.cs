﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshMaster : MonoBehaviour
{
    public FieldCreator original;
    public Vector2Int copySize = new Vector2Int(2, 2);
    List<FieldCreator> copies = new List<FieldCreator>();
    private void Awake()
    {
        copies.Clear();
        UpdateCopies();
    }
    public void DeleteCopies() {
        for (int i = copies.Count-1; i >= 0; i--)
        {
            var toDelete = copies[i];
            copies.Remove(toDelete);
            Destroy(toDelete.gameObject);
        }
    }
    public void UpdateCopies() {
        //destroy children before making new?
        DeleteCopies();
        for (int x = 0; x < copySize.x; x++)
        {
            for (int z = 0; z < copySize.y; z++)
            {
                if ((z == 0 && x == 0) == false)
                {
                    GameObject newInstance = Instantiate(original.gameObject);
                    int pointOffset = (original.sideSize - 1);
                    newInstance.transform.position += Vector3.right * x * pointOffset * original.gridScale;
                    newInstance.transform.position += Vector3.forward * z * pointOffset * original.gridScale;
                    var newField = newInstance.GetComponent<FieldCreator>();
                    newField.offset.x = x * pointOffset;
                    newField.offset.z = z * pointOffset;
                    copies.Add(newField);
                }
            }
        }
    }
}
