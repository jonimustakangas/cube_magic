﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Triangle
{
    public Vector3[] verts;
}
public struct GridCell
{
    public Vector3[] points;
    public float[] values;
}
[RequireComponent(typeof(FieldCreator))]
public class MeshCreator : MonoBehaviour
{
    public Vector3[] vertices = new Vector3[3];
    public int[] triangles = { 0, 1, 2 };
    public bool update = false;
    public bool collision = false;
    public float updateInterval = 0.5f;
    private float lastUpdate = -99f;
    MeshRenderer renderer;
    MeshFilter filter;
    FieldCreator fieldCreator;
    new MeshCollider collider;
    private void Awake()
    {
        renderer = GetComponent<MeshRenderer>();
        fieldCreator = GetComponent<FieldCreator>();
        filter = GetComponent<MeshFilter>();
        vertices = filter.mesh.vertices;
        triangles = filter.mesh.triangles;
        collider = gameObject.AddComponent<MeshCollider>();
    }
    private void Start()
    {
        CreateMesh();
    }
    private void Update()
    {
        if (update && Time.time > lastUpdate + updateInterval)
        {
            lastUpdate = Time.time;
            //ManualMesh();
            CreateMesh();

            collider.enabled = false;//updates it
            collider.enabled = collision;
        }
    }
    public void ManualMesh()
    {
        List<Vector3> verts = new List<Vector3>();
        foreach (var v in vertices)
        {
            verts.Add(v);
        }
        Mesh mesh = filter.mesh;
        mesh.SetVertices(verts);
        mesh.SetTriangles(triangles, 0);
        filter.mesh = mesh;
    }
    public void CreateMesh()
    {
        if (fieldCreator == null)
        {
            Debug.LogError("fieldCreator NULL");
        }
        float iso = fieldCreator.visibilityTreshold;
        float[,,] values = fieldCreator.GetArr();
        Mesh mesh = filter.mesh;
        List<Vector3> allVertices = new List<Vector3>();
        List<int> triIndices = new List<int>();
        int triN = 0;
        for (int x = 0; x < values.GetLength(0) - 1; x++)
        {
            for (int y = 0; y < values.GetLength(1) - 1; y++)
            {
                for (int z = 0; z < values.GetLength(2) - 1; z++)
                {
                    GridCell cell;
                    cell.points = new Vector3[8];
                    cell.values = new float[8];
                    //what's the orientation for vertex indices??
                    for (int i = 0; i < 2; i++)
                    {
                        //assuming rotating x,z
                        cell.points[i * 4] = fieldCreator.GetArrayPos(x, y + i, z,true);
                        cell.points[i * 4 + 1] = fieldCreator.GetArrayPos(x + 1, y + i, z,true);
                        cell.points[i * 4 + 2] = fieldCreator.GetArrayPos(x + 1, y + i, z + 1,true);
                        cell.points[i * 4 + 3] = fieldCreator.GetArrayPos(x, y + i, z + 1,true);

                        cell.values[i * 4] = values[x, y + i, z];
                        cell.values[i * 4 + 1] = values[x + 1, y + i, z];
                        cell.values[i * 4 + 2] = values[x + 1, y + i, z + 1];
                        cell.values[i * 4 + 3] = values[x, y + i, z + 1];
                    }
                    List<Triangle> tris = Polygonize(cell, iso);
                    //add each listof tris to total tri list before applying to mesh?
                    for (int i = 0; i < tris.Count; i++)
                    {
                        foreach (var v in tris[i].verts)
                        {
                            allVertices.Add(v);
                            triIndices.Add(triN);
                            triN++;
                        }
                    }
                }
            }
        }
        //getting error regardless of setting triangles or vertices first, spaghetti fix?
        List<int> shitInit = new List<int>();
        for (int i = 0; i < allVertices.Count; i++)
        {
            shitInit.Add(0);
        }
        mesh.SetTriangles(shitInit.ToArray(),0);
        
        //actually setting the verts and tris
        mesh.SetVertices(allVertices);
        mesh.SetTriangles(triIndices.ToArray(), 0);
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();//?
        List<Vector2> uvs = new List<Vector2>();
        foreach (var v in allVertices)
        {
            uvs.Add(new Vector2(v.x,v.z));

            //uvs.Add(new Vector2(v.x,(v.z+v.y)/2f));
            //uvs.Add(new Vector2(v.x,Mathf.Sqrt(Mathf.Pow(v.y, 2)+Mathf.Pow(v.z,2))));
        }
        mesh.SetUVs(0, uvs);
        filter.mesh = mesh;
    }
    public List<Triangle> Polygonize(GridCell grid, float isoLevel)
    {
        int cubeIndex = 0;
        if (grid.values[0] < isoLevel) cubeIndex |= 1;
        if (grid.values[1] < isoLevel) cubeIndex |= 2;
        if (grid.values[2] < isoLevel) cubeIndex |= 4;
        if (grid.values[3] < isoLevel) cubeIndex |= 8;
        if (grid.values[4] < isoLevel) cubeIndex |= 16;
        if (grid.values[5] < isoLevel) cubeIndex |= 32;
        if (grid.values[6] < isoLevel) cubeIndex |= 64;
        if (grid.values[7] < isoLevel) cubeIndex |= 128;

        Vector3[] vertlist = new Vector3[12];
        /* Find the vertices where the surface intersects the cube */
        if ((Tables.edges[cubeIndex] & 1) > 0)
            vertlist[0] =
               VertexInterp(isoLevel, grid.points[0], grid.points[1], grid.values[0], grid.values[1]);
        if ((Tables.edges[cubeIndex] & 2) > 0)
            vertlist[1] =
               VertexInterp(isoLevel, grid.points[1], grid.points[2], grid.values[1], grid.values[2]);
        if ((Tables.edges[cubeIndex] & 4) > 0)
            vertlist[2] =
               VertexInterp(isoLevel, grid.points[2], grid.points[3], grid.values[2], grid.values[3]);
        if ((Tables.edges[cubeIndex] & 8) > 0)
            vertlist[3] =
               VertexInterp(isoLevel, grid.points[3], grid.points[0], grid.values[3], grid.values[0]);
        if ((Tables.edges[cubeIndex] & 16) > 0)
            vertlist[4] =
               VertexInterp(isoLevel, grid.points[4], grid.points[5], grid.values[4], grid.values[5]);
        if ((Tables.edges[cubeIndex] & 32) > 0)
            vertlist[5] =
               VertexInterp(isoLevel, grid.points[5], grid.points[6], grid.values[5], grid.values[6]);
        if ((Tables.edges[cubeIndex] & 64) > 0)
            vertlist[6] =
               VertexInterp(isoLevel, grid.points[6], grid.points[7], grid.values[6], grid.values[7]);
        if ((Tables.edges[cubeIndex] & 128) > 0)
            vertlist[7] =
               VertexInterp(isoLevel, grid.points[7], grid.points[4], grid.values[7], grid.values[4]);
        if ((Tables.edges[cubeIndex] & 256) > 0)
            vertlist[8] =
               VertexInterp(isoLevel, grid.points[0], grid.points[4], grid.values[0], grid.values[4]);
        if ((Tables.edges[cubeIndex] & 512) > 0)
            vertlist[9] =
               VertexInterp(isoLevel, grid.points[1], grid.points[5], grid.values[1], grid.values[5]);
        if ((Tables.edges[cubeIndex] & 1024) > 0)
            vertlist[10] =
               VertexInterp(isoLevel, grid.points[2], grid.points[6], grid.values[2], grid.values[6]);
        if ((Tables.edges[cubeIndex] & 2048) > 0)
            vertlist[11] =
               VertexInterp(isoLevel, grid.points[3], grid.points[7], grid.values[3], grid.values[7]);

        //create triangle
        List<Triangle> tris = new List<Triangle>();
        for (int i = 0; Tables.tris[cubeIndex, i] != -1; i += 3)
        {
            Triangle newTri;
            newTri.verts = new Vector3[3];
            newTri.verts[0] = vertlist[Tables.tris[cubeIndex, i+2]];
            newTri.verts[1] = vertlist[Tables.tris[cubeIndex, i + 1]];
            newTri.verts[2] = vertlist[Tables.tris[cubeIndex, i]];
            tris.Add(newTri);
        }

        return tris;
    }
    Vector3 VertexInterp(float isolevel, Vector3 a, Vector3 b, float aWeight, float bWeight)
    {
        Vector3 point;
        float lerp;
        float limit = 0.0001f;
        if (Mathf.Abs(isolevel - aWeight) < limit)
        {
            return a;
        }
        if (Mathf.Abs(isolevel - bWeight) < limit)
        {
            return b;
        }
        if (Mathf.Abs(aWeight - bWeight) < limit)
        {
            return a;
        }
        lerp = (isolevel - aWeight) / (bWeight - aWeight);
        Vector3 toB = b - a;
        point = a + lerp * toB;
        return point;
    }
    Vector3 VertexInterp2(float isolevel, Vector3 a, Vector3 b, float aWeight, float bWeight) {
        Vector3 point;
        float limit = 0.0001f;

        if (SmallerThan(b,a))
        {
            //switch a & b around
            Vector3 temp = a;
            float tempWeight = aWeight;
            a = b;
            aWeight = bWeight;
            b = temp;
            bWeight = tempWeight;
        }
        if (Mathf.Abs(aWeight-bWeight) > limit)
        {
            point = a + (b - a) / (bWeight - aWeight) * (isolevel - aWeight);
        }
        else
        {
            point = a;
        }
        return point;
    }
    bool SmallerThan(Vector3 a, Vector3 b) {
        if (a.x < b.x)
        {
            return true;
        }
        else if (a.x > b.x)
        {
            return false;
        }
        if (a.y < b.y)
        {
            return true;
        }
        else if (a.y > b.y)
        {
            return false;
        }
        if (a.z < b.z)
        {
            return true;
        }
        else if (a.z > b.z)
        {
            return false;
        }
        return false;
    }
}
