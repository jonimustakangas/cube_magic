﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class SoundFx : MonoBehaviour
{
    AudioSource source;
    List<float[]> smoothSamples = new List<float[]>();
    List<float> smoothTimes = new List<float>();
    [Range(0, 1)]
    public float smoothTime = 0.2f;
    public Vector2 scale = new Vector2(0.1f, 5f);
    public int sampleSize = 256;
    public float Bscale = 1f;
    public float timeScale = 1f;
    [Range(0, 1)]
    public float historyInterval = 0.5f;
    private float lastHistory = -1;
    public float historyLifeTime = 5f;
    internal List<float[]> smoothedHistory = new List<float[]>();
    List<float> historyTimes = new List<float>();

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //source.GetSpatializerFloat
        //source.GetSpectrumData
        float[] samples = new float[sampleSize];
        source.GetOutputData(samples, 0);
        //Debug.Log(samples[0]);
        smoothSamples.Add(samples);
        smoothTimes.Add(Time.time);
        while (smoothTimes[0] < Time.time - smoothTime)
        {
            smoothTimes.RemoveAt(0);
            smoothSamples.RemoveAt(0);
        }
        //Debug.Log(samples[0]);
        float[] smoothed = GetAvg();
        if (Time.time > lastHistory + historyInterval)
        {
            smoothedHistory.Add(smoothed);
            historyTimes.Add(Time.time);
            lastHistory = Time.time;
        }
        while (Time.time > historyTimes[0] + historyLifeTime)
        {
            historyTimes.RemoveAt(0);
            smoothedHistory.RemoveAt(0);
        }
        DrawHistory();
        //transform.GetChild(0).localScale = Vector3.one * smoothed[0] * 10;
        float relativeScale = scale.x / sampleSize;
        for (int i = 0; i < samples.Length; i++)
        {
            if (samples[i] == 0)
            {
                //Debug.Log("sample " + i + " is 0");
            }
            if (i < smoothed.Length - 1)
            {
                float relI = (float)i / sampleSize;
                float t = Time.time * timeScale;
                Color rainbow = new Color(1 - relI, relI, (smoothed[i] + 0.5f) * Bscale);
                Debug.DrawLine(new Vector3(i * relativeScale, smoothed[i] * scale.y, t), new Vector3((i + 1) * relativeScale, smoothed[i + 1] * scale.y, t), rainbow);
            }
            //if (i < smoothed.Length - 1)
            //{
            //    float relI = (float)i / sampleSize;
            //    Color rainbow = new Color(1 - relI, relI, (smoothed[i] + 0.5f) * Bscale);
            //    Debug.DrawLine(new Vector3(i * relativeScale, smoothSamples[0][i] * scale.y + 2, 0), new Vector3((i + 1) * relativeScale, smoothSamples[0][i + 1] * scale.y + 2, 0), rainbow);
            //}
            //if (i < smoothed.Length - 1)
            //{
            //    float relI = (float)i / sampleSize;
            //    Color rainbow = new Color(1 - relI, relI, (smoothed[i] + 0.5f) * Bscale);
            //    Debug.DrawLine(new Vector3(i * relativeScale, smoothSamples[1][i] * scale.y + 2, 0), new Vector3((i + 1) * relativeScale, smoothSamples[1][i + 1] * scale.y + 2, 0), rainbow);
            //}
        }
    }
    //// void GetAvg<T,C>(T collection) where T : IEnumerable<C>, ICollection<C> where C : struct{
    ////    C dd;
    ////    foreach (var i in collection)
    ////    {
    ////        C d2 = collection[0] + collection[2];
    ////    }
    ////}
    float[] GetAvg()
    {
        float[] sumArr = new float[sampleSize];
        for (int i = 0; i < smoothSamples.Count; i++)
        {
            for (int k = 0; k < sampleSize; k++)
            {
                sumArr[k] += smoothSamples[i][k] / smoothSamples.Count;
            }
        }
        return sumArr;
    }
    private void OnGUI()
    {
        // Camera.main.
    }
    void DrawHistory()
    {
        float relativeScale = scale.x / sampleSize;
        for (int i = 0; i < smoothedHistory.Count; i++)
        {
            for (int a = 0; a < smoothedHistory[i].Length - 1; a++)
            {
                float relI = (float)a / sampleSize;
                float timeMult = 1f-(float)i / smoothedHistory.Count;
                Color rainbow = new Color(1 - relI, relI, (smoothedHistory[i][a] + 0.5f) * Bscale+timeMult);
                Debug.DrawLine(new Vector3(a * relativeScale, smoothedHistory[i][a] * scale.y, historyTimes[i] * timeScale), new Vector3((a + 1) * relativeScale, smoothedHistory[i][a + 1] * scale.y, historyTimes[i] * timeScale), rainbow);
            }
        }
    }
}
