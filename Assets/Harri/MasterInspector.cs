﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(MeshMaster))]
public class MasterInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var master = (MeshMaster)target;
        if (GUILayout.Button("Update"))
        {
            master.UpdateCopies();
        }
    }
}
