﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(FieldCreator))]
public class MeshInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        FieldCreator creator = (FieldCreator)target;
        if (creator.random == FieldCreator.RandomMode.Random)
        {
            if (GUILayout.Button("Randomize"))
            {
                creator.RandomGrid();
            }
        }
        if (creator.random == FieldCreator.RandomMode.RandomBalls)
        {
            if (GUILayout.Button("Randomize"))
            {
                creator.RandomBallGrid();
            }
        }

        //make a meshInspector for this and rename old thing to fieldInspector
        MeshCreator meshCreator = creator.GetComponent<MeshCreator>();
        if (meshCreator && meshCreator.collision)
        {
            if (GUILayout.Button("Populate physics"))
            {
                creator.PopulatePhysics();
            }
        }
    }
}
