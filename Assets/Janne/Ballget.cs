﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pos
{
    public Pos(Vector3 v)
    {
        x = (int)v.x;
        y = (int)v.y;
        z = (int)v.z;
    }

    public int x;
    public int y;
    public int z;
}

public class Ballget : MonoBehaviour
{
    [SerializeField]
    ValueFlowfield3D virtapelto;
    Pos givenPos;

    private void Awake()
    {
        givenPos = new Pos(transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = GetFieldIndexPosition(transform.position);
        if(newPos.x != givenPos.x || newPos.y != givenPos.y || newPos.z != givenPos.z)
        {
            virtapelto.UpdateTarget(newPos);
            givenPos = new Pos(newPos);
        }
    }

    public Vector3 GetFieldIndexPosition(Vector3 position)
    {
        return new Vector3((int)position.x, (int)position.y, (int)position.z) * virtapelto.Scale;
    }
}
