﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidSpawner : MonoBehaviour
{
    public float radius = 1f;

    public float lifetime = 3f;

    public int perFrame = 3;

    public Boid boid;
    public Flowfield3DBase[] target;

    private IEnumerator Start()

    {
        target = FindObjectsOfType<Flowfield3DBase>();
        while (true)

        {

            for (int i = 0; i < perFrame; i++)

            {

                var newBoid = Instantiate(boid, transform.position + Random.insideUnitSphere * radius, Quaternion.identity);
                newBoid.GetComponent<Boid>().lifetime = lifetime;
                newBoid.Target = target[Random.Range(0,target.Length)];

            }

            yield return null;

        }

    }



    private void OnDrawGizmosSelected()

    {

        Gizmos.DrawWireSphere(transform.position, radius);

    }
}
