﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public int cost;
    public int value;
    public int endCost;
    public Vector3 direction;

}

public class ValueFlowfield3D : Flowfield3DBase
{
    int endNodeCost;
    Node[,,] nodeArr;
    float xDir;
    float yDir;
    float zDir;
    Vector3 goalNode;

    [SerializeField]
    bool isDrawing = false;
    [SerializeField]
    bool useBallget = false;
    [SerializeField]
    List<Flowfield3DBase> fields;

    private void Start()
    {
        nodeArr = new Node[Data.Field.GetLength(0), Data.Field.GetLength(1), Data.Field.GetLength(2)];

        for (int x = 0 + (int)gameObject.transform.position.x; x < Data.Field.GetLength(0); x++)
        {
            for (int y = 0 + (int)gameObject.transform.position.y; y < Data.Field.GetLength(1); y++)
            {
                for (int z = 0 + (int)gameObject.transform.position.z; z < Data.Field.GetLength(2); z++)
                {
                    nodeArr[x, y, z] = new Node();
                }
            }
        }
        foreach (Flowfield3DBase flow in GameObject.FindObjectsOfType<Flowfield3DBase>())
        {
            if (flow != this.gameObject.GetComponent<Flowfield3DBase>())
            {
                fields.Add(flow);
            }
        }
        Init();
    }

    public void UpdateTarget(Vector3 pos)
    {
        goalNode = pos;
        recalc();
    }
    void recalc()
    {
        IntegrationField();

        var field = Data.Field;
        for (int x = 0 + (int)gameObject.transform.position.x; x < Data.Field.GetLength(0); x++)
        {
            for (int y = 0 + (int)gameObject.transform.position.y; y < Data.Field.GetLength(1); y++)
            {
                for (int z = 0 + (int)gameObject.transform.position.z; z < Data.Field.GetLength(2); z++)
                {
                    List<Vector3> neighbors = getNeighbors(x, y, z);
                    Vector3 nextNode = neighbors[0];
                    for (int i = 0; i < neighbors.Count; i++)
                    {
                        if (nodeArr[(int)neighbors[i].x, (int)neighbors[i].y, (int)neighbors[i].z].value < nodeArr[(int)nextNode.x, (int)nextNode.y, (int)nextNode.z].value)
                        {
                            nextNode = neighbors[i];
                        }
                    }
                    Vector3 pos = new Vector3(x, y, z);
                    Vector3 direction = new Vector3(pos.x - nextNode.x, pos.y - nextNode.y, pos.z - nextNode.z);
                    if (nodeArr[x, y, z].cost >= 65535)
                    {
                        direction = new Vector3(0, 0, 0);
                    }
                    field[x, y, z] = -direction;

                    //Debug.DrawRay(new Vector3(x, y, z), new Vector3(0, nodeArr[x, y, z].value / 65535, 0));
                }
            }
        }
    }
    void Init()
    {

        int[,,] iArr = FindObjectOfType<FieldCreator>().GetIntArr();
        if (!useBallget)
        {
            int xGoal = Random.Range(0 + (int)gameObject.transform.position.x, Data.Field.GetLength(0));
            int yGoal = Random.Range(0 + (int)gameObject.transform.position.y, Data.Field.GetLength(1));
            int zGoal = Random.Range(0 + (int)gameObject.transform.position.z, Data.Field.GetLength(2));

            while (iArr[xGoal, yGoal, zGoal] == 1)
            {
                xGoal = Random.Range(0 + (int)gameObject.transform.position.x, Data.Field.GetLength(0));
                yGoal = Random.Range(0 + (int)gameObject.transform.position.y, Data.Field.GetLength(1));
                zGoal = Random.Range(0 + (int)gameObject.transform.position.z, Data.Field.GetLength(2));
            }
            goalNode = new Vector3(xGoal, yGoal, zGoal);
        }
        for (int x = 0 + (int)gameObject.transform.position.x; x < Data.Field.GetLength(0); x++)
        {
            for (int y = 0 + (int)gameObject.transform.position.y; y < Data.Field.GetLength(1); y++)
            {
                for (int z = 0 + (int)gameObject.transform.position.z; z < Data.Field.GetLength(2); z++)
                {
                    //nodeArr[x, y, z].cost = Random.Range(1, 255);
                    if (iArr[x, y, z] == 1)
                    {
                        nodeArr[x, y, z].cost = 65535;
                    }
                    else
                    {
                        nodeArr[x, y, z].cost = 1;// Random.Range(1, 255);
                    }
                    if (x == (int)goalNode.x && y == (int)goalNode.y && z == (int)goalNode.z)
                    {
                        nodeArr[x, y, z].cost = 0;
                    }

                }
            }
        }

        IntegrationField();

        var field = Data.Field;
        for (int x = 0 + (int)gameObject.transform.position.x; x < Data.Field.GetLength(0); x++)
        {
            for (int y = 0 + (int)gameObject.transform.position.y; y < Data.Field.GetLength(1); y++)
            {
                for (int z = 0 + (int)gameObject.transform.position.z; z < Data.Field.GetLength(2); z++)
                {
                    List<Vector3> neighbors = getNeighbors(x, y, z);
                    Vector3 nextNode = neighbors[0];
                    for (int i = 0; i < neighbors.Count; i++)
                    {
                        if (nodeArr[(int)neighbors[i].x, (int)neighbors[i].y, (int)neighbors[i].z].value < nodeArr[(int)nextNode.x, (int)nextNode.y, (int)nextNode.z].value)
                        {
                            nextNode = neighbors[i];
                        }
                    }
                    Vector3 pos = new Vector3(x, y, z);
                    Vector3 direction = new Vector3(pos.x - nextNode.x, pos.y - nextNode.y, pos.z - nextNode.z);
                    if (nodeArr[x, y, z].cost >= 65535)
                    {
                        direction = new Vector3(0, 0, 0);
                    }
                    field[x, y, z] = -direction;

                    //Debug.DrawRay(new Vector3(x, y, z), new Vector3(0, nodeArr[x, y, z].value / 65535, 0));
                }
            }
        }

        Data.SetAll(field);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Init();
        }

        if (isDrawing) Draw();
    }
    void IntegrationField()
    {
        //Set total cost in all cells to 65535
        //resetField();
        List<Vector3> openList = new List<Vector3>();
        //nodeArr[(int)targetV3.x, (int)targetV3.y, (int)targetV3.z].value = 0;
        openList.Add(goalNode);


        //Set goal node cost to 0 and add it to the open list
        resetField();

        //Get the next node in the open list
        while (openList.Count > 0)
        {
            Vector3 currentVec = openList[0];
            openList.RemoveAt(0);

            int currentX = (int)currentVec.x;
            int currentY = (int)currentVec.y;
            int currentZ = (int)currentVec.z;
            //Debug.Log("x = " + currentX + ", y = " + currentY + ", z = " + currentZ);

            //Get the N, E, S, and W neighbors of the current node
            List<Vector3> neighbors = getNeighbors(currentX, currentY, currentZ);
            int neighborCount = neighbors.Count;

            //Iterate through the neighbors of the current node
            for (int i = 0; i < neighborCount; i++)
            {
                //Calculate the new cost of the neighbor node             
                // based on the cost of the current node and the weight of the next node 
                //int endNodeCost = getValueByIndex(currentID) + getCostField().getCostByIndex(neighbors[i]);
                int endNodeCost = nodeArr[currentX, currentY, currentZ].value + nodeArr[(int)neighbors[i].x, (int)neighbors[i].y, (int)neighbors[i].z].cost;

                //Debug.Log(baseNodeArr[currentX, currentY, currentZ]);
                //Debug.Log(nodeArr[currentX, currentY, currentZ]);

                //If a shorter path has been found, add the node into the open list
                if (endNodeCost < nodeArr[(int)neighbors[i].x, (int)neighbors[i].y, (int)neighbors[i].z].value)
                {
                    //Check if the neighbor cell is already in the list.
                    //If it is not then add it to the end of the list.
                    if (!openList.Contains(neighbors[i]))
                    {
                        openList.Add(neighbors[i]);
                    }
                    //Set the new cost of the neighbor node.
                    nodeArr[(int)neighbors[i].x, (int)neighbors[i].y, (int)neighbors[i].z].value = endNodeCost;
                }
            }
        }
    }
    List<Vector3> getNeighbors(int xPos, int yPos, int zPos)
    {
        List<Vector3> neighbors = new List<Vector3>();
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    // Go through shit
                    Vector3 CurPos = new Vector3(xPos + x, yPos + y, zPos + z);
                    if (CurPos.x >= 0 + (int)gameObject.transform.position.x && CurPos.x < Data.Field.GetLength(0))
                    {
                        if (CurPos.y >= 0 + (int)gameObject.transform.position.y && CurPos.y < Data.Field.GetLength(1))
                        {
                            if (CurPos.z >= 0 + (int)gameObject.transform.position.z && CurPos.z < Data.Field.GetLength(2))
                            {
                                neighbors.Add(CurPos);
                            }
                        }
                    }

                }
            }
        }
        return neighbors;
    }

    void resetField()
    {
        for (int x = 0 + (int)gameObject.transform.position.x; x < Data.Field.GetLength(0); x++)
        {
            for (int y = 0 + (int)gameObject.transform.position.y; y < Data.Field.GetLength(1); y++)
            {
                for (int z = 0 + (int)gameObject.transform.position.z; z < Data.Field.GetLength(2); z++)
                {
                    nodeArr[x, y, z].value = 65535;
                }
            }
        }
        nodeArr[(int)goalNode.x, (int)goalNode.y, (int)goalNode.z].value = 0;
    }

}
